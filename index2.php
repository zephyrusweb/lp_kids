<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <div class="label"></div>
        <section class="b01" id="b01">
            <div class="container">
                <h1>БЕСПЛАТНЫЕ ВИДЕО-УРОКИ РИСОВАНИЯ <span>ДЛЯ ДЕТЕЙ 5-8 ЛЕТ</span></h1>
            </div>
            <div class="content">
                <div class="container">
                    <div class="center-block">
                        <div class="text">
                            <div class="text-inner">Ваш ребенок научится рисовать фигуру человека, разнообразную технику, наряды для кукол, лица людей, и разные прически!</div>
                        </div>
                        <div class="center-block-inner">
                            <div class="content-title"><span>Как получить бесплатные<br/>видео-уроки:</span></div>
                            <ol class="advantage">
                                <li>Выберите форму подписки для девочек или для мальчиков</li>
                                <li>Введите ваше имя и адрес электронной почты</li>
                                <li>Нажмите на кноку "Получить бесплатные уроки"</li>
                                <li>Загляните в почту и подтвердите подписку</li>
                            </ol>
                        </div>
                    </div>
                    <div class="left-col">
                        <div class="form-block form-girl">
                            <div class="form-title">Введите свои данные и получите 8 видео-уроков из серии<br/>"Рисуем наряды для принцессы Анечки"</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-7;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для девочек">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="form-block form-boy">
                            <div class="form-title">Введите свои данные  и получите 6 видео-уроков из серии<br/>"Рисуем технику с мальчишками"</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-5;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для мальчиков">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid name none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="b02 short" id="b02">

        </section>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.pack.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/main.js"></script>

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter35379845 = new Ya.Metrika({id:35379845,
                            webvisor:true,
                            clickmap:true,
                            trackHash:true});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/35379845" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </body>
</html>
