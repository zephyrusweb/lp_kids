<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>
        <nav class="topnav">
            <div class="container">
                <div class="navbar-header">
                    <span class="navbar-title navbar-toggle"  data-target=".navbar">Меню</span>
                    <span class="navbar-button  navbar-toggle" data-target=".navbar"><i class="fa fa-navicon"></i></span>
                </div>
                <ul class="navbar">
                    <li><a href="#b01">Получить уроки!</a></li>
                    <li><a href="#b03">Рисунки</a></li>
                    <li><a href="#b05">Отзывы</a></li>
                    <li><a href="#b02">Зачем всё это?</a></li>
                </ul>
            </div>
        </nav>
        <div class="label"></div>
        <section class="b01" id="b01">
            <div class="container">
                <h1>БЕСПЛАТНЫЕ ВИДЕО-УРОКИ РИСОВАНИЯ <span>ДЛЯ ДЕТЕЙ 5-8 ЛЕТ</span></h1>
            </div>
            <div class="content">
                <div class="container">
                    <div class="center-block">
                        <div class="text">
                            <div class="text-inner">Ваш ребенок научится рисовать фигуру человека, разнообразную технику, наряды для кукол, лица людей, и разные прически!</div>
                        </div>
                        <div class="center-block-inner">
                            <div class="content-title"><span>Как получить бесплатные<br/>видео-уроки:</span></div>
                            <ol class="advantage">
                                <li>Выберите форму подписки для девочек или для мальчиков</li>
                                <li>Введите ваше имя и адрес электронной почты</li>
                                <li>Нажмите на кноку "Получить бесплатные уроки"</li>
                                <li>Загляните в почту и подтвердите подписку</li>
                            </ol>
                        </div>
                    </div>
                    <div class="left-col">
                        <div class="form-block form-girl">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для девочек</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-7;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для девочек">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="form-block form-boy">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для мальчиков</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-5;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для мальчиков">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid name none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="b02" id="b02">
            <div class="container">
                <h2>Почему вам и вашим детям <span>понравятся наши видео-уроки</span></h2>
                <ul class="features">
                    <li class="item1">
                        <img src="img/b02_item1.png" alt="">
                        <div class="item-title">Каждому свой курс</div>
                        <p>У мальчиков и девочек разные интересы и склонности. Девочки чаще любят живопись, кукол, а мальчики – графику, машины и технику. Поэтому мы постарались учесть все пожелания и интересы наших зрителей и разработали отдельные программы для мальчиков и девочек.</p>
                    </li>
                    <li class="item2">
                        <img src="img/b02_item2.png" alt="">
                        <div class="item-title">Творческое развитие потенциала детей</div>
                        <p>Не всегда есть время и возможность возить детей по кружкам. Но теперь благодаря техническим возможностям  это и не нужно, ваш ребенок может развиваться не выходя из собственной комнаты, в удобной для него атмосфере и в удобное время. Все, что для этого нужно – подготовить все необходимые материалы и включить компьютер с нашими видео-уроками!</p>
                    </li>
                    <li class="item3">
                        <img src="img/b02_item3.png" alt="">
                        <div class="item-title">Воспитание думающего ребенка</div>
                        <p>Худшее, что может случиться — это если ребенок выполнит точную копию работы педагога. Наши задания построены таким образом, чтобы ребенок, руководствуясь рекомендациями, мог придумать и воплотить на листе бумаги свои собственные фантазии!</p>
                    </li>
                </ul>
            </div>
        </section>

        <section class="b03" id="b03">
            <div class="container">
                <h2>Рисунки наших учеников</h2>
                <div class="gallery">
                    <div class="row">
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img01.jpg" class="gallery-img btn-modal">
                                <img src="images/img01m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img02.jpg" class="gallery-img btn-modal">
                                <img src="images/img02m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img03.jpg" class="gallery-img btn-modal">
                                <img src="images/img03m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img04.jpg" class="gallery-img btn-modal">
                                <img src="images/img04m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img05.jpg" class="gallery-img btn-modal">
                                <img src="images/img05m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img06.jpg" class="gallery-img btn-modal">
                                <img src="images/img06m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img07.jpg" class="gallery-img btn-modal">
                                <img src="images/img07m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img08.jpg" class="gallery-img btn-modal">
                                <img src="images/img08m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img09.jpg" class="gallery-img btn-modal">
                                <img src="images/img09m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img10.jpg" class="gallery-img btn-modal">
                                <img src="images/img10m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img11.jpg" class="gallery-img btn-modal">
                                <img src="images/img11m.jpg" class="img-responsive">
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a rel="gallery" href="images/img12.jpg" class="gallery-img btn-modal">
                                <img src="images/img12m.jpg" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="b04">
            <div class="container">
                <div class="benefit" id="b04">
                    <h2>Итак, что Вы получите:</h2>
                    <ol class="clearfix">
                        <li>Любознательного ребенка, который готов не только учиться рисованию, но и познавать окружающий мир</li>
                        <li>До 4х часов в неделю, сэкономленных на сборах и дороге до кружка,  которые Вы, можете потратить на себя</li>
                        <li>Несколько рисунков и поделок, которые можно торжественно демонстрировать друзьям, доказывая художественный талант Вашего чада. Или подарить бабушкам</li>
                        <li>Вам не нужно занимать голову вопросом «Чем занять ребенка сегодня?».  Вы получаете  30 — 60 минутное, полноценное занятие от профессионального педагога тогда, когда это удобно Вам, а не педагогу</li>
                        <li>Если что-то пойдет не так, у Вас всегда есть поддержка. Присылайте рисунки, задавайте вопросы, мы обязательно поможем Вам.</li>
                    </ol>
                </div>
            </div>

            <div class="content">
                <div class="container">
                    <div class="center-block">
                        <div class="text">
                            <div class="text-inner">Ваш ребенок научится рисовать фигуру человека, разнообразную технику, наряды для кукол, лица людей, и разные прически!</div>
                        </div>
                        <div class="center-block-inner">
                            <div class="content-title"><span>Как получить бесплатные<br/>видео-уроки:</span></div>
                            <ol class="advantage">
                                <li>Выберите форму подписки для девочек или для мальчиков</li>
                                <li>Введите ваше имя и адрес электронной почты</li>
                                <li>Нажмите на кноку "Получить бесплатные уроки"</li>
                                <li>Загляните в почту и подтвердите подписку</li>
                            </ol>
                        </div>
                    </div>
                    <div class="left-col">
                        <div class="form-block form-girl">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для девочек</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-7;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для девочек">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="form-block form-boy">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для мальчиков</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-5;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для мальчиков">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid name none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="b05" id="b05">
            <div class="container">
                <h2>Отзывы наших учеников</h2>
                <div class="review">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="images/review/review01.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review01.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review02.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review02.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review03.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review03.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review04.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review04.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review05.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review05.jpg" class="img-responsive" alt="">
                            </a>
                        </div>

                        <div class="col-sm-6">
                            <a href="images/review/review06.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review06.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review07.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review07.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review08.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review08.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review09.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review09.jpg" class="img-responsive" alt="">
                            </a>

                            <a href="images/review/review10.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review10.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                    </div>

                    <a href="images/review/review11.jpg" class="review-item btn-modal" rel="review">
                        <img src="images/review/review11.jpg" class="img-responsive" alt="">
                    </a>

                    <a href="images/review/review12.jpg" class="review-item btn-modal" rel="review">
                        <img src="images/review/review12.jpg" class="img-responsive" alt="">
                    </a>

                    <a href="images/review/review13.jpg" class="review-item btn-modal" rel="review">
                        <img src="images/review/review13.jpg" class="img-responsive" alt="">
                    </a>

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="images/review/review14.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review14.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="images/review/review15.jpg" class="review-item btn-modal" rel="review">
                                <img src="images/review/review15.jpg" class="img-responsive" alt="">
                            </a>
                        </div>
                    </div>

                    <a href="images/review/review16.jpg" class="review-item btn-modal" rel="review">
                        <img src="images/review/review16.jpg" class="img-responsive" alt="">
                    </a>

                    <a href="images/review/review17.jpg" class="review-item btn-modal" rel="review">
                        <img src="images/review/review17.jpg" class="img-responsive" alt="">
                    </a>
                </div>
            </div>
        </section>

        <section class="b06" id="b06">
            <div class="content">
                <div class="container">
                    <div class="center-block">
                        <div class="text">
                            <div class="text-inner">Ваш ребенок научится рисовать фигуру человека, разнообразную технику, наряды для кукол, лица людей, и разные прически!</div>
                        </div>
                        <div class="center-block-inner">
                            <div class="content-title"><span>Как получить бесплатные<br/>видео-уроки:</span></div>
                            <ol class="advantage">
                                <li>Выберите форму подписки для девочек или для мальчиков</li>
                                <li>Введите ваше имя и адрес электронной почты</li>
                                <li>Нажмите на кноку "Получить бесплатные уроки"</li>
                                <li>Загляните в почту и подтвердите подписку</li>
                            </ol>
                        </div>
                    </div>
                    <div class="left-col">
                        <div class="form-block form-girl">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для девочек</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-7;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для девочек">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                    <div class="right-col">
                        <div class="form-block form-boy">
                            <div class="form-title">Введите свои данные и получите серию бесплатных видео-уроков по рисованию для мальчиков</div>
                            <form class="form" name="form" method="POST" action="javascript:void(0);">
                                <input type="hidden" name="id" value="urokirisovaniy-5;" />
                                <input type="hidden" name="refid" value="" />
                                <input type="hidden" name="channel" value="" />
                                <!-- Скрытые поля, которые содержат данные из UTM метки -->
                                <input type="hidden" name="info1" class="info1" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>" />
                                <input type="hidden" name="info2" class="info2" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>" />

                                <input name="info3" type="hidden" value="Заявка видео-уроков для мальчиков">

                                <div class="form-group valid name none required">
                                    <input type="text" name="name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="form-group valid name none required">
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email">
                                </div>
                                <button type="submit" class="btn btn-send  onclick="yaCounter35379845.reachGoal('ORDER'); return true;"">Получить<br/>бесплатные уроки</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.pack.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/main.js"></script>

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter35379845 = new Ya.Metrika({id:35379845,
                            webvisor:true,
                            clickmap:true,
                            trackHash:true});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/35379845" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </body>
</html>
