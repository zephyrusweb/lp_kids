// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var navbar = $('.topnav');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
        navbar.toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });

    $('.topnav li a').click(function() {
        var w = $(window).width();
        if(w < 768) {
            menu.hide();
            navbar.removeClass('open');
        }
    });
});


$('.topnav li a').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset:-50 });
    return false;
});


$(function($){
    var topnav = $('.topnav');
    var label = $('.label');
    $h = label.offset().top;

    $(window).scroll(function(){

        if ( $(window).scrollTop() > $h) {
            topnav.addClass('fix-top');
        }else{
            topnav.removeClass('fix-top');
        }
    });
});


// Modal


$(".btn-modal").fancybox({
    'padding'    : 0,
    'closeBtn'   : false
});


$(document).ready(function() {

    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form   = $(this).closest('form'),
                id      =    $('input[name="id"]', $form).val(),
                refid   =    $('input[name="refid"]', $form).val(),
                channel =    $('input[name="channel"]', $form).val(),
                name    =    $('input[name="name"]', $form).val(),
                email   =    $('input[name="email"]', $form).val(),

                info1   =    $('input[name="info1"]', $form).val(),
                info2   =    $('input[name="info2"]', $form).val(),
                info3   =    $('input[name="info3"]', $form).val();

            console.log(name, email, id, refid, channel, info1, info2, info3);

            $.ajax({
                type: "POST",
                dataType: 'jsonp',
                timeout: 30*1000,
                cache: false,
                async: true,
                contentType: "application/json",
                crossDomain: true,
                jsonpCallback: 'test_responce',
                url: "http://ect-subscribe.com/subscribe/",
                data: {name: name, email: email, id: id, refid: refid, channel: channel, info1: info1, info2: info2, info3: info3 },
                success: function(data) {
                    alert("Запрос выполнен");
                }
            });

            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {name: name, email: email, id: id, refid: refid, channel: channel, info1: info1, info2: info2, info3: info3 }
            }).done(function(msg) {
                console.log(name, email, id, refid, channel, info1, info2, info3);
                console.log('удачно на форму');


                $('form').find('input[type=text], textarea').val('');
                $.fancybox(
                    '<div class="done">'+ '<span class="done-title">Спасибо, Ваша заявка принята!</span>' +'</div>',
                    {
                        'autoDimensions'  : false,
                        'padding': 0,
                        'minWidth': 600,
                        'transitionIn'    : 'none',
                        'transitionOut'   : 'none'
                    }
                );
                setTimeout("$.fancybox.close()", 3000);
            });
        }

    });
});
